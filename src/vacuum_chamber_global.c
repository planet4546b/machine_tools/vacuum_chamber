#include "vacuum_chamber_config.h"
#include "vacuum_chamber_global.h"
#include "logger.h"

#include "brightness_api.h"
#include "pump_api.h"
#include "valves_api.h"
#include "buzzer_api.h"
#include "vibration_api.h"

#include "FreeRTOS.h"
#include "task.h"

static vc_dev_t vc_dev = { 0 };

void vc_init_global_struct(void)
{
    /* Prepare for degassing */
    valves_turn_off_all_mosfets();

    pump_off();
    vc_set_flag(PUMP_STATE, 0);

    pump_valve_open();
    vc_set_flag(VALVE_PUMP_STATE, 1);

    bleed_valve_close();
    vc_set_flag(BLEED_PUMP_STATE, 0);

    motors_off();

    logger_dgb_print("[LL] vacuum chamber global init done\r\n");
}

/* Uptime API */
void vc_inc_uptime(void) { ++vc_dev.uptime; };
uint32_t vc_get_uptime(void) { return vc_dev.uptime; }

void vc_set_flag(vc_flags_t flag, int new_state)
{
    taskENTER_CRITICAL();
    {
        if (new_state == VC_FLAG_OFF) { /* Reset flag */
            vc_dev.flags &= ~flag;
        } else { /* Set flag */
            vc_dev.flags |= flag;
        }
    }
    taskEXIT_CRITICAL();
}

int vc_get_flag(vc_flags_t flag)
{
    return (vc_dev.flags & flag);
}

void vc_set_x_vibro(uint8_t percentage)
{
    taskENTER_CRITICAL();
    {
        vc_dev.vibro_x_percentage = percentage;
        motor_x_set_power(0, percentage);
    }
    taskEXIT_CRITICAL();

#if 1
    logger_dgb_print("[vibro][X] %d%%\r\n", percentage);
#endif
}

void vc_set_y_vibro(uint8_t percentage)
{
    taskENTER_CRITICAL();
    {
        vc_dev.vibro_y_percentage = percentage;
        motor_y_set_power(0, percentage);
    }
    taskEXIT_CRITICAL();

#if 1
    logger_dgb_print("[vibro][Y] %d%%\r\n", percentage);
#endif
}

uint8_t vc_get_x_vibro(void) { return vc_dev.vibro_x_percentage; }
uint8_t vc_get_y_vibro(void) { return vc_dev.vibro_y_percentage; }
