#include "vacuum_chamber_config.h"
#include "errors.h"
#include "logger.h"

void fw_assert_func(int assert_num, int is_critical)
{
    int iter = 0;

    logger_dgb_print("[ERR]: %s ASSERT_%d\n",
                     (is_critical == ASSERT_CRITICAL) ? "CRITICAL" : "non critical",
                     assert_num);

    if (is_critical) {
        while (1) {
            ++iter;

            HAL_GPIO_TogglePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin);
            HAL_Delay(50);

            if (iter == 20) {
                HAL_NVIC_SystemReset();
            }
        }
    }
}
