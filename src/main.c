#include "vacuum_chamber_config.h"
#include "peripheral_init.h"
#include "logger.h"
#include "errors.h"

#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"

#include "non_os_delay.h"
#include "vacuum_chamber_global.h"

#include "debug_thread.h"
#include "gui_thread.h"

#include "ui.h"

#include "buzzer_api.h"
#include "brightness_api.h"
#include "push_btn_api.h"
#include "pump_api.h"
#include "valves_api.h"
#include "vibration_api.h"
#include "encoder_api.h"

int main(void)
{
    /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
    HAL_Init();

    /* Configure the system clock */
    clock_init();

    /* FreeRTOS will setup SysTick by itself.
       We get HardFault when SysTick IRQ has happened before the scheduler started.
       So, we MUST disable it. Otherwise 1ms probably elapsed if we use uart logger
       function for instance and we got HF.
    */
    systick_disable_int();

    /* No OS delay to make HAL_Delay() alive and init counter for 1ms less delays */
    no_os_delay_timer_init_irq();

    /* Initialize all configured cpu peripherals */
    gpio_init();
    rtc_init();
    spi3_init();
    tim2_init();
    tim3_init();
    uart1_init();
    logger_dgb_print("[LL] cpu peripherals init done\r\n");

    /* Initialize all configured board peripherals */
    vc_init_global_struct();
    st7789_adapter_init();

    /* Initialize all libs */
    lv_init();
    lvgl_lcd_adapter_init();
    logger_dgb_print("[LL] LVGL init done\r\n");

    /* Create all RTOS threads */
#if (DEBUG_TASK_USE == 1)
    debug_thread_create();
#endif
    gui_thread_create();

    /* This "Hello" from the FW means that all necessary initializations successfully done.
       Otherwise we got some messages from FW_ASSERT() and than system reset.
       Check the error.h module in common repo. */
    logger_dgb_print("[Vacuum-chamber] FW started. CPU clock = %d Hz\n", SystemCoreClock);

    /* Disable 1ms IRQ for hal_delay, because after OS start, we use vApplicationTickHook() for inc tick */
    no_os_delay_timer_disable_irq();

    /* FW started */
#if BUZZER_USE_START_FW_BEEPING
    buzzer_make_ms_beep(BUZZER_START_FW_BEEP_DURATION);
#endif

    /* Start the scheduler. */
    vTaskStartScheduler();

    /* Will only get here if there was insufficient memory to create the idle
    task. The idle task is created within vTaskStartScheduler().
    We use static allocation, but leave it here for the future */
    for ( ;; ) {
        logger_dgb_print("[Vacuum-chamber ERR] FreeRTOS insufficient memory!\n");
        HAL_GPIO_TogglePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin);
        HAL_Delay(50);
    }
}
