#include "vacuum_chamber_config.h"
#include "vacuum_chamber_global.h"
#include "ui.h"

static lv_obj_t *ui_hello_screen;
static lv_obj_t *ui_logo_img;
static lv_obj_t *ui_device_name;
static lv_obj_t *ui_fw_version;

void hello_screen_init(void)
{
    ui_hello_screen = lv_obj_create(NULL);
    lv_obj_clear_flag(ui_hello_screen, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_set_style_bg_color(ui_hello_screen, lv_color_hex(0x000000), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_opa(ui_hello_screen, 255, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_logo_img = lv_img_create(ui_hello_screen);
    lv_img_set_src(ui_logo_img, &planet4546_logo100x100);
    lv_obj_set_width(ui_logo_img, LV_SIZE_CONTENT);
    lv_obj_set_height(ui_logo_img, LV_SIZE_CONTENT);
    lv_obj_set_x(ui_logo_img, 0);
    lv_obj_set_y(ui_logo_img, -50);
    lv_obj_set_align(ui_logo_img, LV_ALIGN_CENTER);
    lv_obj_add_flag(ui_logo_img, LV_OBJ_FLAG_ADV_HITTEST);
    lv_obj_clear_flag(ui_logo_img, LV_OBJ_FLAG_SCROLLABLE);

    ui_device_name = lv_label_create(ui_hello_screen);
    lv_obj_set_width(ui_device_name, LV_SIZE_CONTENT);
    lv_obj_set_height(ui_device_name, LV_SIZE_CONTENT);
    lv_obj_set_x(ui_device_name, 0);
    lv_obj_set_y(ui_device_name, 50);
    lv_obj_set_align(ui_device_name, LV_ALIGN_CENTER);
    lv_label_set_text_static(ui_device_name, "ДЕГАЗАТОР 3000");
    lv_obj_set_style_text_color(ui_device_name, lv_color_hex(0xffffff), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_device_name, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_device_name, &JetBrainsMonoNL_SemiBold_20, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_fw_version = lv_label_create(ui_hello_screen);
    lv_obj_set_width(ui_fw_version, LV_SIZE_CONTENT);
    lv_obj_set_height(ui_fw_version, LV_SIZE_CONTENT);
    lv_obj_set_x(ui_fw_version, 0);
    lv_obj_set_y(ui_fw_version, 70);
    lv_obj_set_align(ui_fw_version, LV_ALIGN_CENTER);
    lv_label_set_text_static(ui_fw_version, "Версия 0.0.1 (test)");
    lv_obj_set_style_text_color(ui_fw_version, lv_color_hex(0xff0000), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_fw_version, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_fw_version, &JetBrainsMonoNL_Regular_14, LV_PART_MAIN | LV_STATE_DEFAULT);
}

void hello_screen_show(void)
{
    lv_disp_load_scr(ui_hello_screen);
}
