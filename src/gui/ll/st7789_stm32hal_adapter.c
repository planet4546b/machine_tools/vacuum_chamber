#include "st7789.h"
#include "st7789_stm32hal_adapter.h"
#include "non_os_delay.h"
#include "vacuum_chamber_config.h"
#include "vacuum_chamber_global.h"
#include "errors.h"
#include "logger.h"

static void st7789_adapter_delay_ms(uint32_t ms) { no_os_delay_us(ms); }

/* Reset LCD with hardware pin (optional) */
static void st7789_adapter_rst_pin_set(void) { LCD_RESET_GPIO_Port->BSRR = LCD_RESET_Pin; }
static void st7789_adapter_rst_pin_reset(void) { LCD_RESET_GPIO_Port->BSRR = LCD_RESET_Pin << 16; }

/* Manage chip select pin */
static void st7789_adapter_cs_pin_set(void) { LCD_SPI_CS_GPIO_Port->BSRR = LCD_SPI_CS_Pin; }
static void st7789_adapter_cs_pin_reset(void) { LCD_SPI_CS_GPIO_Port->BSRR = LCD_SPI_CS_Pin << 16; }

/*
 * Manage D/C pin in case of 4-lines serial interface.
 *
 * Pin low -> data is treated as command
 * Pin high -> data is treated as parameter or data
 */
static void st7789_adapter_dc_pin_set(void) { LCD_DS_GPIO_Port->BSRR = LCD_DS_Pin; }
static void st7789_adapter_dc_pin_reset(void) { LCD_DS_GPIO_Port->BSRR = LCD_DS_Pin << 16; }

#if (ST7789_LCD_USE_DMA == 1)
static void dma_spi_reload(void *data, uint32_t len, uint32_t memory_width)
{
    dma_single_data_parameter_struct dma_init_struct;

    /* configure SPI1 transmit dma */
    dma_deinit(DMA0, DMA_CH4);
    dma_init_struct.periph_addr         = (uint32_t)&SPI_DATA(SPI1);
    dma_init_struct.memory0_addr        = (uint32_t)data;
    dma_init_struct.direction           = DMA_MEMORY_TO_PERIPH;
    dma_init_struct.periph_memory_width = memory_width;
    dma_init_struct.priority            = DMA_PRIORITY_LOW;
    dma_init_struct.number              = len;
    dma_init_struct.periph_inc          = DMA_PERIPH_INCREASE_DISABLE;
    dma_init_struct.memory_inc          = DMA_MEMORY_INCREASE_ENABLE;
    dma_init_struct.circular_mode       = DMA_CIRCULAR_MODE_DISABLE;
    dma_single_data_mode_init(DMA0, DMA_CH4, &dma_init_struct);
    dma_channel_subperipheral_select(DMA0, DMA_CH4, DMA_SUBPERI0);
}

__attribute__((optimize("-O0"))) static void spi_dma_send_data_16(uint16_t *buf, size_t len)
{
    while(!dma_flag_get(DMA0, DMA_CH4, DMA_FLAG_FTF) && spi_i2s_flag_get(LCD_SPI, SPI_FLAG_TRANS)) {}

    while (len > 0)
    {
        uint32_t chunk_size = len > 0xFFFF ? 0xFFFF : len;

        dma_spi_reload(buf, chunk_size, DMA_PERIPH_WIDTH_16BIT);
        dma_channel_enable(DMA0, DMA_CH4);
        spi_dma_enable(LCD_SPI, SPI_DMA_TRANSMIT);

        /* wait dma transmit complete */
        while(!dma_flag_get(DMA0, DMA_CH4, DMA_FLAG_FTF) && spi_i2s_flag_get(LCD_SPI, SPI_FLAG_TRANS)) {}

        buf += chunk_size;
        len -= chunk_size;
    }
}

__attribute__((optimize("-O0"))) static void spi_dma_send_data_8(uint8_t *buf, size_t len)
{
    while(!dma_flag_get(DMA0, DMA_CH4, DMA_FLAG_FTF) && spi_i2s_flag_get(LCD_SPI, SPI_FLAG_TRANS)) {}

    while (len > 0)
    {
        uint32_t chunk_size = len > 0xFFFF ? 0xFFFF : len;

        dma_spi_reload(buf, chunk_size, DMA_PERIPH_WIDTH_8BIT);
        dma_channel_enable(DMA0, DMA_CH4);
        spi_dma_enable(LCD_SPI, SPI_DMA_TRANSMIT);

        /* wait dma transmit complete */
        while(!dma_flag_get(DMA0, DMA_CH4, DMA_FLAG_FTF) && spi_i2s_flag_get(LCD_SPI, SPI_FLAG_TRANS)) {}

        buf += chunk_size;
        len -= chunk_size;
    }
}
#endif

/* SPI data transfer */
__attribute__((optimize("-O0"))) static void st7789_adapter_spi_send_data8(uint8_t *buf, size_t len)
{
    extern SPI_HandleTypeDef hspi3;

    if (hspi3.Init.DataSize == SPI_DATASIZE_16BIT) {
        __HAL_SPI_DISABLE(&hspi3);
        hspi3.Init.DataSize = SPI_DATASIZE_8BIT;
        SPI3->CR1 &= ~(SPI_CR1_DFF); /* Bit 11 DFF: Data frame format */
        __HAL_SPI_ENABLE(&hspi3);
    }

    if (HAL_SPI_Transmit(&hspi3, buf, (uint16_t)len, 100) != HAL_OK) {
        FW_ASSERT(1, LCD_SPI_TRANSFER_ASSERT_1010, ASSERT_NON_CRITICAL);
    }
}

/* SPI data transfer in 16 bit frame mode */
__attribute__((optimize("-O0"))) static void st7789_adapter_spi_send_data16(uint16_t *buf, size_t len)
{
    extern SPI_HandleTypeDef hspi3;

    if (hspi3.Init.DataSize == SPI_DATASIZE_8BIT) {
        __HAL_SPI_DISABLE(&hspi3);
        hspi3.Init.DataSize = SPI_DATASIZE_16BIT;
        SPI3->CR1 |= SPI_CR1_DFF; /* Bit 11 DFF: Data frame format */
        __HAL_SPI_ENABLE(&hspi3);
    }

    if (HAL_SPI_Transmit(&hspi3, (uint8_t *)buf, (uint16_t)len, 100) != HAL_OK) {
        FW_ASSERT(1, LCD_SPI_TRANSFER_ASSERT_1011, ASSERT_NON_CRITICAL);
    }
}

static st7789_ll_t st7789_ll =   
{
    .delay_ms = st7789_adapter_delay_ms,
    .rst_pin_set = st7789_adapter_rst_pin_set,
    .rst_pin_reset = st7789_adapter_rst_pin_reset,
    .cs_pin_set = st7789_adapter_cs_pin_set,
    .cs_pin_reset = st7789_adapter_cs_pin_reset,
    .dc_pin_set = st7789_adapter_dc_pin_set,
    .dc_pin_reset = st7789_adapter_dc_pin_reset,
    .spi_send_data8 = st7789_adapter_spi_send_data8,
    .spi_send_data16 = st7789_adapter_spi_send_data16,
};

static st7789_dev_t st7789_lcd_device = { 0 };

void *st7789_adapter_get_ll_func(void) { return (void *)&st7789_ll; }
void *st7789_adapter_get_dev(void) { return (void *)&st7789_lcd_device; }
void st7789_adapter_set_rotation(st7789_rotation_t rot) { st7789_set_rotation(&st7789_lcd_device, rot); }

void st7789_adapter_init(void)
{
    st7789_init(st7789_adapter_get_dev(),
                st7789_adapter_get_ll_func());

#if 0 /* While have no lvgl fill with smth to prevent garbage on LCD after reset */
    st7789_fill_color(st7789_adapter_get_dev(), ST7789_MAGENTA_RGB565);
#endif

#if 0 /* Test st7789 controller without lvgl */
    while (1) {
        /* Clear sreen */
        st7789_fill_color(st7789_adapter_get_dev(), ST7789_RED_RGB565);
        HAL_Delay(1000);
        st7789_fill_color(st7789_adapter_get_dev(), ST7789_GREEN_RGB565);
        HAL_Delay(1000);
        st7789_fill_color(st7789_adapter_get_dev(), ST7789_BLUE_RGB565);
        HAL_Delay(1000);
        st7789_fill_color(st7789_adapter_get_dev(), ST7789_BLACK_RGB565);

        /* Test set-pizel function: draw square 1px thickness */
        for (int i = 100; i < 200; ++i) {
            st7789_set_pixel(st7789_adapter_get_dev(), i, 100, ST7789_WHITE_RGB565);
            st7789_set_pixel(st7789_adapter_get_dev(), 100, i, ST7789_WHITE_RGB565);
            st7789_set_pixel(st7789_adapter_get_dev(), 200, i, ST7789_WHITE_RGB565);
            st7789_set_pixel(st7789_adapter_get_dev(), i, 200, ST7789_WHITE_RGB565);
        }
    }
#endif

    logger_dgb_print("[LL] st7789 lcd controller init done\r\n");
}
