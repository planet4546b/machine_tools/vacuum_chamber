#include "lvgl_lcd_adapter.h"
#include "lvgl.h"
#include "vacuum_chamber_config.h"
#include "vacuum_chamber_global.h"
#include "st7789.h"
#include "st7789_stm32hal_adapter.h"
#include "errors.h"
#include "logger.h"

static lv_display_t *display = NULL;
static lv_color16_t buf1[320 * 240 / 10] = { 0 };

#if LVGL_USE_PRINT_LOG
static void lvgl_log_function(lv_log_level_t level, const char * buf)
{
    (void)level;
    logger_dgb_print("[lvgl] %s\r\n", buf);
}
#endif

static void flush_cb(lv_display_t *disp, const lv_area_t *area, uint8_t *px_map)
{
    st7789_fill_area_with_raw_data(st7789_adapter_get_dev(),
                                   area->x1, area->y1,
                                   area->x2, area->y2,
                                   (uint16_t *)px_map,
                                   lv_area_get_width(area) * lv_area_get_height(area));

    lv_display_flush_ready(disp); /* Indicate you are ready with the flushing */
}

void lvgl_lcd_adapter_init(void)
{
#if LVGL_USE_PRINT_LOG
    lv_log_register_print_cb(lvgl_log_function);
#endif

    display = lv_display_create(((st7789_dev_t *)st7789_adapter_get_dev())->width,
                                ((st7789_dev_t *)st7789_adapter_get_dev())->heigh);
    FW_ASSERT(display == NULL, LCD_LVGL_ASSERT_1016, ASSERT_CRITICAL);

    lv_display_set_color_format(display, LV_COLOR_FORMAT_RGB565);
    lv_display_set_buffers(display, buf1, NULL, sizeof(buf1), LV_DISPLAY_RENDER_MODE_PARTIAL);
    lv_display_set_flush_cb(display, flush_cb);
}
