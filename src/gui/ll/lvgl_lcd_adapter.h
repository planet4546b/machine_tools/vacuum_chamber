#ifndef __LVGL_LCD_ADAPTER_H
#define __LVGL_LCD_ADAPTER_H

#include "st7789.h"

/* Init display from LVGL point of view and also frame buffers */
void lvgl_lcd_adapter_init(void);

#endif /* __LVGL_LCD_ADAPTER_H */
