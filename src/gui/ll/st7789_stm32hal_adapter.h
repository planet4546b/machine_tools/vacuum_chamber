#ifndef __SS7789_ADAPTER_H
#define __SS7789_ADAPTER_H

#include "st7789.h"

void *st7789_adapter_get_ll_func(void);
void *st7789_adapter_get_dev(void);
void st7789_adapter_set_rotation(st7789_rotation_t rot);

void st7789_adapter_init(void);

#endif /* __SS7789_ADAPTER_H */
