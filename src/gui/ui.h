#ifndef __VACUUM_CHAMBER_UI_H
#define __VACUUM_CHAMBER_UI_H

#include "lvgl.h"

#include "gui/ll/st7789_stm32hal_adapter.h"
#include "gui/ll/lvgl_lcd_adapter.h"

#include "hello_screen.h"
#include "main_screen.h"

/* Images */
LV_IMAGE_DECLARE(planet4546_logo100x100);

/* Fonts */
LV_FONT_DECLARE(JetBrainsMonoNL_Regular_14);
LV_FONT_DECLARE(JetBrainsMonoNL_SemiBold_20);

void ui_update_degassing_state(const char *text);
void ui_update_x_vibro(void);

#endif /* __VACUUM_CHAMBER_UI_H */
