#ifndef __HELLO_SCREEN_H
#define __HELLO_SCREEN_H

void hello_screen_init(void);
void hello_screen_show(void);

#endif /* __HELLO_SCREEN_H */
