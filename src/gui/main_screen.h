#ifndef __MAIN_SCREEN_H
#define __MAIN_SCREEN_H

void main_screen_init(void);
void main_screen_show(void);

#endif /* __MAIN_SCREEN_H */
