#include "vacuum_chamber_config.h"
#include "vacuum_chamber_global.h"
#include "ui.h"

static lv_obj_t * ui_main_screen;
static lv_obj_t * ui_degassing;
static lv_obj_t * ui_valves;
static lv_obj_t * ui_vertical_line;
static lv_obj_t * ui_vibrotable;
static lv_obj_t * ui_settings;
static lv_obj_t * ui_y_line;
static lv_obj_t * ui_Y;
static lv_obj_t * ui_x_line;
static lv_obj_t * ui_X;
static lv_obj_t * ui_X_value;
static lv_obj_t * ui_Y_value;
static lv_obj_t * ui_mixture_settings;
static lv_obj_t * ui_device_settings;
static lv_obj_t * ui_valve_pump;
static lv_obj_t * ui_valve_bleed;
static lv_obj_t * ui_degassing_state;
static lv_obj_t * ui_degassing_time;
static lv_obj_t * ui_start;

void main_screen_init(void)
{
    ui_main_screen = lv_obj_create(NULL);
    lv_obj_clear_flag(ui_main_screen, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_set_style_bg_color(ui_main_screen, lv_color_hex(0x000000), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_opa(ui_main_screen, 255, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_degassing = lv_label_create(ui_main_screen);
    lv_obj_set_width(ui_degassing, LV_SIZE_CONTENT);
    lv_obj_set_height(ui_degassing, LV_SIZE_CONTENT);
    lv_obj_set_x(ui_degassing, -83);
    lv_obj_set_y(ui_degassing, -87);
    lv_obj_set_align(ui_degassing, LV_ALIGN_CENTER);
    lv_label_set_text_static(ui_degassing, "Дегазация");
    lv_obj_set_style_text_color(ui_degassing, lv_color_hex(0xFFFFFF), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_degassing, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_degassing, &JetBrainsMonoNL_SemiBold_20, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_border_color(ui_degassing, lv_color_hex(0xFFFFFF), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_border_opa(ui_degassing, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_border_width(ui_degassing, 2, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_border_side(ui_degassing, LV_BORDER_SIDE_BOTTOM, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_valves = lv_label_create(ui_main_screen);
    lv_obj_set_width(ui_valves, LV_SIZE_CONTENT);
    lv_obj_set_height(ui_valves, LV_SIZE_CONTENT);
    lv_obj_set_x(ui_valves, 90);
    lv_obj_set_y(ui_valves, -87);
    lv_obj_set_align(ui_valves, LV_ALIGN_CENTER);
    lv_label_set_text_static(ui_valves, "Клапана");
    lv_obj_set_style_text_color(ui_valves, lv_color_hex(0xFFFFFF), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_valves, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_valves, &JetBrainsMonoNL_SemiBold_20, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_border_color(ui_valves, lv_color_hex(0xFFFFFF), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_border_opa(ui_valves, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_border_width(ui_valves, 2, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_border_side(ui_valves, LV_BORDER_SIDE_BOTTOM, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_vertical_line = lv_label_create(ui_main_screen);
    lv_obj_set_height(ui_vertical_line, 170);
    lv_obj_set_width(ui_vertical_line, LV_SIZE_CONTENT);
    lv_obj_set_x(ui_vertical_line, 22);
    lv_obj_set_y(ui_vertical_line, 9);
    lv_obj_set_align(ui_vertical_line, LV_ALIGN_CENTER);
    lv_label_set_text(ui_vertical_line, "");
    lv_obj_set_style_border_color(ui_vertical_line, lv_color_hex(0xFFFFFF), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_border_opa(ui_vertical_line, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_border_width(ui_vertical_line, 2, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_border_side(ui_vertical_line, LV_BORDER_SIDE_LEFT, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_vibrotable = lv_label_create(ui_main_screen);
    lv_obj_set_width(ui_vibrotable, LV_SIZE_CONTENT);
    lv_obj_set_height(ui_vibrotable, LV_SIZE_CONTENT);
    lv_obj_set_x(ui_vibrotable, -83);
    lv_obj_set_y(ui_vibrotable, 34);
    lv_obj_set_align(ui_vibrotable, LV_ALIGN_CENTER);
    lv_label_set_text_static(ui_vibrotable, "Вибростол");
    lv_obj_set_style_text_color(ui_vibrotable, lv_color_hex(0xFFFFFF), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_vibrotable, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_vibrotable, &JetBrainsMonoNL_SemiBold_20, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_border_color(ui_vibrotable, lv_color_hex(0xFFFFFF), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_border_opa(ui_vibrotable, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_border_width(ui_vibrotable, 2, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_border_side(ui_vibrotable, LV_BORDER_SIDE_BOTTOM, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_settings = lv_label_create(ui_main_screen);
    lv_obj_set_width(ui_settings, LV_SIZE_CONTENT);
    lv_obj_set_height(ui_settings, LV_SIZE_CONTENT);
    lv_obj_set_x(ui_settings, 90);
    lv_obj_set_y(ui_settings, 34);
    lv_obj_set_align(ui_settings, LV_ALIGN_CENTER);
    lv_label_set_text_static(ui_settings, "Настройки");
    lv_obj_set_style_text_color(ui_settings, lv_color_hex(0xFFFFFF), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_settings, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_settings, &JetBrainsMonoNL_SemiBold_20, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_border_color(ui_settings, lv_color_hex(0xFFFFFF), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_border_opa(ui_settings, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_border_width(ui_settings, 2, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_border_side(ui_settings, LV_BORDER_SIDE_BOTTOM, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_y_line = lv_label_create(ui_main_screen);
    lv_obj_set_height(ui_y_line, 45);
    lv_obj_set_width(ui_y_line, LV_SIZE_CONTENT);
    lv_obj_set_x(ui_y_line, -120);
    lv_obj_set_y(ui_y_line, 81);
    lv_obj_set_align(ui_y_line, LV_ALIGN_CENTER);
    lv_label_set_text(ui_y_line, "");
    lv_obj_set_style_border_color(ui_y_line, lv_color_hex(0xE0FF3A), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_border_opa(ui_y_line, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_border_width(ui_y_line, 2, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_border_side(ui_y_line, LV_BORDER_SIDE_LEFT, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_Y = lv_label_create(ui_main_screen);
    lv_obj_set_width(ui_Y, LV_SIZE_CONTENT);
    lv_obj_set_height(ui_Y, LV_SIZE_CONTENT);
    lv_obj_set_x(ui_Y, -131);
    lv_obj_set_y(ui_Y, 61);
    lv_obj_set_align(ui_Y, LV_ALIGN_CENTER);
    lv_label_set_text(ui_Y, "Y");
    lv_obj_set_style_text_color(ui_Y, lv_color_hex(0xE0FF3A), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_Y, 255, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_x_line = lv_label_create(ui_main_screen);
    lv_obj_set_width(ui_x_line, 45);
    lv_obj_set_height(ui_x_line, LV_SIZE_CONTENT);
    lv_obj_set_x(ui_x_line, -99);
    lv_obj_set_y(ui_x_line, 95);
    lv_obj_set_align(ui_x_line, LV_ALIGN_CENTER);
    lv_label_set_text(ui_x_line, "");
    lv_obj_set_style_border_color(ui_x_line, lv_color_hex(0xE0FF3A), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_border_opa(ui_x_line, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_border_width(ui_x_line, 2, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_border_side(ui_x_line, LV_BORDER_SIDE_BOTTOM, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_X = lv_label_create(ui_main_screen);
    lv_obj_set_width(ui_X, LV_SIZE_CONTENT);
    lv_obj_set_height(ui_X, LV_SIZE_CONTENT);
    lv_obj_set_x(ui_X, -69);
    lv_obj_set_y(ui_X, 106);
    lv_obj_set_align(ui_X, LV_ALIGN_CENTER);
    lv_label_set_text(ui_X, "X");
    lv_obj_set_style_text_color(ui_X, lv_color_hex(0xE0FF3A), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_X, 255, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_X_value = lv_label_create(ui_main_screen);
    lv_obj_set_width(ui_X_value, LV_SIZE_CONTENT);
    lv_obj_set_height(ui_X_value, LV_SIZE_CONTENT);
    lv_obj_set_x(ui_X_value, -22);
    lv_obj_set_y(ui_X_value, 65);
    lv_obj_set_align(ui_X_value, LV_ALIGN_CENTER);
    lv_label_set_text_fmt(ui_X_value, "X: %d%%", vc_get_x_vibro());
    lv_obj_set_style_text_color(ui_X_value, lv_color_hex(0xE0FF3A), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_X_value, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_align(ui_X_value, LV_TEXT_ALIGN_LEFT, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_X_value, &JetBrainsMonoNL_Regular_14, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_Y_value = lv_label_create(ui_main_screen);
    lv_obj_set_width(ui_Y_value, LV_SIZE_CONTENT);
    lv_obj_set_height(ui_Y_value, LV_SIZE_CONTENT);
    lv_obj_set_x(ui_Y_value, -22);
    lv_obj_set_y(ui_Y_value, 86);
    lv_obj_set_align(ui_Y_value, LV_ALIGN_CENTER);
    lv_label_set_text_fmt(ui_Y_value, "Y: %d%%", vc_get_y_vibro());
    lv_obj_set_style_text_color(ui_Y_value, lv_color_hex(0xE0FF3A), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_Y_value, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_align(ui_Y_value, LV_TEXT_ALIGN_LEFT, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_Y_value, &JetBrainsMonoNL_Regular_14, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_mixture_settings = lv_label_create(ui_main_screen);
    lv_obj_set_width(ui_mixture_settings, LV_SIZE_CONTENT);
    lv_obj_set_height(ui_mixture_settings, LV_SIZE_CONTENT);
    lv_obj_set_x(ui_mixture_settings, 60);
    lv_obj_set_y(ui_mixture_settings, 65);
    lv_obj_set_align(ui_mixture_settings, LV_ALIGN_CENTER);
    lv_label_set_text_static(ui_mixture_settings, "Смесь");
    lv_obj_set_style_text_color(ui_mixture_settings, lv_color_hex(0x0000FF), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_mixture_settings, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_mixture_settings, &JetBrainsMonoNL_Regular_14, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_color(ui_mixture_settings, lv_color_hex(0xFFFFFF), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_opa(ui_mixture_settings, 255, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_device_settings = lv_label_create(ui_main_screen);
    lv_obj_set_width(ui_device_settings, LV_SIZE_CONTENT);
    lv_obj_set_height(ui_device_settings, LV_SIZE_CONTENT);
    lv_obj_set_x(ui_device_settings, 79);
    lv_obj_set_y(ui_device_settings, 85);
    lv_obj_set_align(ui_device_settings, LV_ALIGN_CENTER);
    lv_label_set_text_static(ui_device_settings, "Устройство");
    lv_obj_set_style_text_color(ui_device_settings, lv_color_hex(0x0000FF), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_device_settings, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_device_settings, &JetBrainsMonoNL_Regular_14, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_color(ui_device_settings, lv_color_hex(0xFFFFFF), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_opa(ui_device_settings, 255, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_valve_pump = lv_label_create(ui_main_screen);
    lv_obj_set_width(ui_valve_pump, LV_SIZE_CONTENT);
    lv_obj_set_height(ui_valve_pump, LV_SIZE_CONTENT);
    lv_obj_set_x(ui_valve_pump, 84);
    lv_obj_set_y(ui_valve_pump, -47);
    lv_obj_set_align(ui_valve_pump, LV_ALIGN_CENTER);
    lv_label_set_text_fmt(ui_valve_pump, "Насос: %s", vc_get_flag(VALVE_PUMP_STATE) ? "открыт" : "закрыт");
    lv_obj_set_style_text_color(ui_valve_pump, lv_color_hex(0x00FF00), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_valve_pump, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_align(ui_valve_pump, LV_TEXT_ALIGN_LEFT, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_valve_pump, &JetBrainsMonoNL_Regular_14, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_valve_bleed = lv_label_create(ui_main_screen);
    lv_obj_set_width(ui_valve_bleed, LV_SIZE_CONTENT);
    lv_obj_set_height(ui_valve_bleed, LV_SIZE_CONTENT);
    lv_obj_set_x(ui_valve_bleed, 85);
    lv_obj_set_y(ui_valve_bleed, -25);
    lv_obj_set_align(ui_valve_bleed, LV_ALIGN_CENTER);
    lv_label_set_text_fmt(ui_valve_bleed, "Спуск: %s", vc_get_flag(BLEED_PUMP_STATE) ? "открыт" : "закрыт");
    lv_obj_set_style_text_color(ui_valve_bleed, lv_color_hex(0x00FF00), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_valve_bleed, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_align(ui_valve_bleed, LV_TEXT_ALIGN_LEFT, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_valve_bleed, &JetBrainsMonoNL_Regular_14, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_degassing_state = lv_label_create(ui_main_screen);
    lv_obj_set_width(ui_degassing_state, LV_SIZE_CONTENT);
    lv_obj_set_height(ui_degassing_state, LV_SIZE_CONTENT);
    lv_obj_set_x(ui_degassing_state, -82);
    lv_obj_set_y(ui_degassing_state, -60);
    lv_obj_set_align(ui_degassing_state, LV_ALIGN_CENTER);
    lv_label_set_text(ui_degassing_state, "ожидание команд");
    lv_obj_set_style_text_color(ui_degassing_state, lv_color_hex(0xFF0000), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_degassing_state, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_degassing_state, &JetBrainsMonoNL_Regular_14, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_degassing_time = lv_label_create(ui_main_screen);
    lv_obj_set_width(ui_degassing_time, LV_SIZE_CONTENT);
    lv_obj_set_height(ui_degassing_time, LV_SIZE_CONTENT);
    lv_obj_set_x(ui_degassing_time, -88);
    lv_obj_set_y(ui_degassing_time, -18);
    lv_obj_set_align(ui_degassing_time, LV_ALIGN_CENTER);
    lv_label_set_text(ui_degassing_time, "Время: 0 сек");
    lv_obj_set_style_text_color(ui_degassing_time, lv_color_hex(0xFF0000), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_degassing_time, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_align(ui_degassing_time, LV_TEXT_ALIGN_LEFT, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_degassing_time, &JetBrainsMonoNL_Regular_14, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_start = lv_label_create(ui_main_screen);
    lv_obj_set_width(ui_start, LV_SIZE_CONTENT);
    lv_obj_set_height(ui_start, LV_SIZE_CONTENT);
    lv_obj_set_x(ui_start, -114);
    lv_obj_set_y(ui_start, 2);
    lv_obj_set_align(ui_start, LV_ALIGN_CENTER);
    lv_label_set_text(ui_start, "Старт");
    lv_obj_set_style_text_color(ui_start, lv_color_hex(0xFF0000), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_start, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_align(ui_start, LV_TEXT_ALIGN_LEFT, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_start, &JetBrainsMonoNL_Regular_14, LV_PART_MAIN | LV_STATE_DEFAULT);
}

void main_screen_show(void)
{
    lv_disp_load_scr(ui_main_screen);
}


void ui_update_degassing_state(const char *text)
{
    lv_label_set_text(ui_degassing_state, text);
}

void ui_update_x_vibro(void)
{
    lv_label_set_text_fmt(ui_X_value, "X: %d%%", vc_get_x_vibro());
}
