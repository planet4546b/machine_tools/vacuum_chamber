#ifndef __LOGGER_H
#define __LOGGER_H

#ifdef __cplusplus
extern "C" {
#endif

void logger_dgb_print(const char *format, ...);

#ifdef __cplusplus
}
#endif

#endif /* __LOGGER_H */
