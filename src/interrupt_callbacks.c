#include "stm32f4xx_it.h"
#include "vacuum_chamber_config.h"
#include "vacuum_chamber_global.h"
#include "encoder_api.h"

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    /* The timer is used only before RTOS scheduler start
     * to provide HAL_Delay() service */
    if (htim->Instance == TIM10) {
        HAL_IncTick();
    }
}

void HAL_RTCEx_WakeUpTimerEventCallback(RTC_HandleTypeDef *hrtc)
{
    if (hrtc->Instance == RTC) {
        vc_inc_uptime();
    }
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    if (GPIO_Pin == ENCODER_A_Pin) {
        encoder_isr();
    } else if (GPIO_Pin == ENCODER_B_Pin) {
        encoder_isr();
    }
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
    if (huart->Instance == USART1) {
        ;
    }
}
