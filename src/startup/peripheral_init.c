#include "vacuum_chamber_config.h"
#include "peripheral_init.h"
#include "errors.h"

#include "FreeRTOSConfig.h"

RTC_HandleTypeDef hrtc;
SPI_HandleTypeDef hspi3;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim10;
UART_HandleTypeDef huart1;

void HAL_MspInit(void)
{
    __HAL_RCC_SYSCFG_CLK_ENABLE();
    __HAL_RCC_PWR_CLK_ENABLE();
}

void clock_init(void)
{
    HAL_StatusTypeDef ret = HAL_OK;
    RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };
    RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };

    /** Configure the main internal regulator output voltage
     */
    __HAL_RCC_PWR_CLK_ENABLE();
    __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

    /** Initializes the RCC Oscillators according to the specified parameters
     * in the RCC_OscInitTypeDef structure.
     */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE|RCC_OSCILLATORTYPE_LSE;
    RCC_OscInitStruct.HSEState = RCC_HSE_ON;
    RCC_OscInitStruct.LSEState = RCC_LSE_ON;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
    RCC_OscInitStruct.PLL.PLLM = 25;
    RCC_OscInitStruct.PLL.PLLN = 168;
    RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
    RCC_OscInitStruct.PLL.PLLQ = 6;
    ret = HAL_RCC_OscConfig(&RCC_OscInitStruct);
    FW_ASSERT(ret != HAL_OK, CLOCK_INIT_ASSERT_0, ASSERT_CRITICAL);

    /** Initializes the CPU, AHB and APB buses clocks
     */
    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                                |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

/*
    RM0368:
    Adaptive real-time memory accelerator (ART Accelerator™)

    The proprietary Adaptive real-time (ART) memory accelerator is optimized for STM32
    industry-standard Arm® Cortex®-M4 with FPU processors. It balances the inherent
    performance advantage of the Arm® Cortex®-M4 with FPU over Flash memory
    technologies, which normally requires the processor to wait for the Flash memory at higher
    operating frequencies.

    To release the processor full performance, the accelerator implements an instruction
    prefetch queue and branch cache which increases program execution speed from the 128-
    bit Flash memory. Based on CoreMark benchmark, the performance achieved thanks to the
    ART accelerator is equivalent to 0 wait state program execution from Flash memory at a
    CPU frequency up to 84 MHz.

    This featire was enabled in HAL_Init();
    Theoretically we can use FLASH_LATENCY_0, but CubeMX generate FLASH_LATENCY_2.
    Need deep DS reading and investigation.
*/
    ret = HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2);
    FW_ASSERT(ret != HAL_OK, CLOCK_INIT_ASSERT_1, ASSERT_CRITICAL);
}

void tim2_init(void)
{
    GPIO_InitTypeDef GPIO_InitStruct = { 0 };
    HAL_StatusTypeDef ret = HAL_OK;
    TIM_MasterConfigTypeDef sMasterConfig = { 0 };
    TIM_OC_InitTypeDef sConfigOC = { 0 };

    __HAL_RCC_TIM2_CLK_ENABLE();

    htim2.Instance = TIM2;
    htim2.Init.Prescaler = 84 - 1;
    htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim2.Init.Period = 100; /* 10 KHz */
    htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
    ret = HAL_TIM_PWM_Init(&htim2);
    FW_ASSERT(ret != HAL_OK, TIM2_INIT_ASSERT_15, ASSERT_CRITICAL);

    sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
    sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
    ret = HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig);
    FW_ASSERT(ret != HAL_OK, TIM2_INIT_ASSERT_16, ASSERT_CRITICAL);

    sConfigOC.OCMode = TIM_OCMODE_PWM1;
    sConfigOC.Pulse = 0;
    sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
    sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;

    ret = HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_1); /* LCD PWM */
    FW_ASSERT(ret != HAL_OK, TIM2_INIT_ASSERT_17, ASSERT_CRITICAL);

    ret = HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_3); /* X engine RPWM */
    FW_ASSERT(ret != HAL_OK, TIM2_INIT_ASSERT_18, ASSERT_CRITICAL);

    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();
    /**TIM2 GPIO Configuration
    PA0-WKUP     ------> TIM2_CH1
    PB10     ------> TIM2_CH3
    */
    GPIO_InitStruct.Pin = LCD_BRIGHT_PWM_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Alternate = GPIO_AF1_TIM2;
    HAL_GPIO_Init(LCD_BRIGHT_PWM_GPIO_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = X_RPWM_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Alternate = GPIO_AF1_TIM2;
    HAL_GPIO_Init(X_RPWM_GPIO_Port, &GPIO_InitStruct);

    ret = HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
    FW_ASSERT(ret != HAL_OK, LCD_PWM_START_ASSERT_1000, ASSERT_CRITICAL);

    ret = HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_3);
    FW_ASSERT(ret != HAL_OK, ENGINES_PWM_START_ASSERT_1005, ASSERT_CRITICAL);
}

void uart1_init(void)
{
    GPIO_InitTypeDef GPIO_InitStruct = { 0 };
    HAL_StatusTypeDef ret = HAL_OK;

    __HAL_RCC_USART1_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();

    /**USART1 GPIO Configuration
    PA9     ------> USART1_TX
    PA10     ------> USART1_RX
    */
    GPIO_InitStruct.Pin = GPIO_PIN_9 | GPIO_PIN_10;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF7_USART1;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    HAL_NVIC_SetPriority(USART1_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(USART1_IRQn);

    huart1.Instance = USART1;
    huart1.Init.BaudRate = DEBUG_UART_SPEED;
    huart1.Init.WordLength = UART_WORDLENGTH_8B;
    huart1.Init.StopBits = UART_STOPBITS_1;
    huart1.Init.Parity = UART_PARITY_NONE;
    huart1.Init.Mode = UART_MODE_TX_RX;
    huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    huart1.Init.OverSampling = UART_OVERSAMPLING_16;
    ret = HAL_UART_Init(&huart1);
    FW_ASSERT(ret != HAL_OK, UART1_INIT_ASSERT_25, ASSERT_CRITICAL);
}

void gpio_init(void)
{
    GPIO_InitTypeDef GPIO_InitStruct = { 0 };

    /* GPIO Ports Clock Enable */
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOH_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();

    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOA, LCD_DS_Pin | BUZZER_Pin | LCD_SPI_CS_Pin | LCD_RESET_Pin, GPIO_PIN_RESET);

    /* Close P-Ch MOSFETs */
    HAL_GPIO_WritePin(GPIOB, BLEED_VALVE_CLOSE_Pin | BLEED_VALVE_OPEN_Pin | PUMP_VALVE_CLOSE_Pin | PUMP_VALVE_OPEN_Pin, GPIO_PIN_SET);

    /* Turn off relay */
    HAL_GPIO_WritePin(GPIOA, PUMP_ON_Pin, GPIO_PIN_SET);

    /*Configure GPIO pin : DEBUG_LED_Pin */
    GPIO_InitStruct.Pin = DEBUG_LED_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(DEBUG_LED_GPIO_Port, &GPIO_InitStruct);

    /*Configure GPIO pins : LCD_DS_Pin BUZZER_Pin */
    GPIO_InitStruct.Pin = LCD_DS_Pin | BUZZER_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /*Configure GPIO pins : ENCODER_SW_Pin RED_BTN_Pin */
    GPIO_InitStruct.Pin = ENCODER_SW_Pin | RED_BTN_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /*Configure GPIO pins : ENCODER_A_Pin ENCODER_B_Pin */
    GPIO_InitStruct.Pin = ENCODER_A_Pin | ENCODER_B_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    HAL_NVIC_SetPriority(EXTI3_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(EXTI3_IRQn);
    HAL_NVIC_SetPriority(EXTI4_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(EXTI4_IRQn);

    /*Configure GPIO pins : BLEED_VALVE_CLOSE_Pin BLEED_VALVE_OPEN_Pin PUMP_VALVE_CLOSE_Pin PUMP_VALVE_OPEN_Pin */
    GPIO_InitStruct.Pin = BLEED_VALVE_CLOSE_Pin | BLEED_VALVE_OPEN_Pin | PUMP_VALVE_CLOSE_Pin | PUMP_VALVE_OPEN_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    /*Configure GPIO pin : PUMP_ON_Pin */
    GPIO_InitStruct.Pin = PUMP_ON_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(PUMP_ON_GPIO_Port, &GPIO_InitStruct);

    /*Configure GPIO pin : LCD_SPI_CS_Pin */
    GPIO_InitStruct.Pin = LCD_SPI_CS_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(LCD_SPI_CS_GPIO_Port, &GPIO_InitStruct);

    /*Configure GPIO pin : LCD_RESET_Pin */
    GPIO_InitStruct.Pin = LCD_RESET_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(LCD_RESET_GPIO_Port, &GPIO_InitStruct);
}

void rtc_init(void)
{
    HAL_StatusTypeDef ret = HAL_OK;
    RTC_TimeTypeDef sTime = { 0 };
    RTC_DateTypeDef sDate = { 0 };
    RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = { 0 };

    PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_RTC;
    PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
    ret = HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct);
    FW_ASSERT(ret != HAL_OK, RTC_INIT_ASSERT_9, ASSERT_CRITICAL);

    __HAL_RCC_RTC_ENABLE();
    HAL_NVIC_SetPriority(RTC_WKUP_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(RTC_WKUP_IRQn);

    hrtc.Instance = RTC;
    hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
    hrtc.Init.AsynchPrediv = 127;
    hrtc.Init.SynchPrediv = 255;
    hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
    hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
    hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
    ret = HAL_RTC_Init(&hrtc);
    FW_ASSERT(ret != HAL_OK, RTC_INIT_ASSERT_5, ASSERT_CRITICAL);

    sTime.Hours = 0x0;
    sTime.Minutes = 0x0;
    sTime.Seconds = 0x0;
    sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
    sTime.StoreOperation = RTC_STOREOPERATION_RESET;
    ret = HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BCD);
    FW_ASSERT(ret != HAL_OK, RTC_INIT_ASSERT_6, ASSERT_CRITICAL);

    sDate.WeekDay = RTC_WEEKDAY_MONDAY;
    sDate.Month = RTC_MONTH_JANUARY;
    sDate.Date = 0x1;
    sDate.Year = 0x0;
    ret = HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BCD);
    FW_ASSERT(ret != HAL_OK, RTC_INIT_ASSERT_7, ASSERT_CRITICAL);

    /** Enable the WakeUp */
    ret = HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, 0, RTC_WAKEUPCLOCK_CK_SPRE_16BITS);
    FW_ASSERT(ret != HAL_OK, RTC_INIT_ASSERT_8, ASSERT_CRITICAL);
}

void spi3_init(void)
{
    HAL_StatusTypeDef ret = HAL_OK;
    GPIO_InitTypeDef GPIO_InitStruct = { 0 };

    __HAL_RCC_SPI3_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();

    /**SPI3 GPIO Configuration
    PB3     ------> SPI3_SCK
    PB5     ------> SPI3_MOSI
    */
    GPIO_InitStruct.Pin = LCD_SPI_SCK_Pin | LCD_SPI_MOSI_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF6_SPI3;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    hspi3.Instance = SPI3;
    hspi3.Init.Mode = SPI_MODE_MASTER;
    hspi3.Init.Direction = SPI_DIRECTION_1LINE;
    hspi3.Init.DataSize = SPI_DATASIZE_8BIT;
    hspi3.Init.CLKPolarity = SPI_POLARITY_HIGH;
    hspi3.Init.CLKPhase = SPI_PHASE_2EDGE;
    hspi3.Init.NSS = SPI_NSS_SOFT;
    hspi3.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
    hspi3.Init.FirstBit = SPI_FIRSTBIT_MSB;
    hspi3.Init.TIMode = SPI_TIMODE_DISABLE;
    hspi3.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
    hspi3.Init.CRCPolynomial = 10;
    ret = HAL_SPI_Init(&hspi3);
    FW_ASSERT(ret != HAL_OK, SPI3_INIT_ASSERT_10, ASSERT_CRITICAL);
}

void tim10_counter_init(int use_irq)
{
    HAL_StatusTypeDef ret = HAL_OK;

    __HAL_RCC_TIM10_CLK_ENABLE();

    /* disable timer */
    TIM10->DIER &= ~(TIM_IT_UPDATE);
    TIM10->CR1 &= ~(TIM_CR1_CEN);

    htim10.Instance = TIM10;
    htim10.Init.Prescaler = 84 - 1;
    htim10.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim10.Init.Period = use_irq ? 1000 : 65535; // 1 ms interrupt or just max counter to do less 1ms delays
    htim10.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    htim10.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
    ret = HAL_TIM_Base_Init(&htim10);
    FW_ASSERT(ret != HAL_OK, TIM10_INIT_ASSERT_30, ASSERT_CRITICAL);

    if (use_irq) {
        /* This timer is used to generate 1ms for HAL_Delay().
        For this we need the interrupt enabled. Therefore TIM1_UP_TIM10_IRQn priority must be more
        than masked by Freertos.

        (FreeRTOS maSkes interrupts lower then
        configMAX_SYSCALL_INTERRUPT_PRIORITY before the scheduler starts)!!
        Actually RTOS don't UNMASK previously masked IRQ.

        For example we called xTaskCreateStatic() first time.
        uxCriticalNesting variable hasn't been initialized yet. It is done by the xPortStartScheduler().
        uxCriticalNesting is static variable which value is 0xaaaaaaaa.
        Therefore function vPortExitCritical() believes that we have nested calls for entering
        ctirical sectoin. Might be it is a BUG. Might be not. Anyway scheduler initialize the
        variable during start. But for know we must remember that if we want use interrupts
        before OS start (f.i. to implement increment for HAL_Delay function) we MUST
        use priority larger than interrupt's priority masked by OS.

         ! For Cortex-M3 the lower number the larger proirity !
        */

        HAL_NVIC_SetPriority(TIM1_UP_TIM10_IRQn, (configMAX_SYSCALL_INTERRUPT_PRIORITY >> __NVIC_PRIO_BITS) - 1, 0);
        HAL_NVIC_EnableIRQ(TIM1_UP_TIM10_IRQn);
        ret = HAL_TIM_Base_Start_IT(&htim10);
    } else {
        HAL_NVIC_DisableIRQ(TIM1_UP_TIM10_IRQn);
        ret = HAL_TIM_Base_Start(&htim10);
    }

    FW_ASSERT(ret != HAL_OK, TIM10_INIT_ASSERT_31, ASSERT_CRITICAL);
}

void systick_disable_int(void)
{
    SysTick->CTRL &= ~(SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_TICKINT_Msk| SysTick_CTRL_ENABLE_Msk);
}

void tim3_init(void)
{
    HAL_StatusTypeDef ret = HAL_OK;
    GPIO_InitTypeDef GPIO_InitStruct = { 0 };
    TIM_MasterConfigTypeDef sMasterConfig = { 0 };
    TIM_OC_InitTypeDef sConfigOC = { 0 };

    __HAL_RCC_TIM3_CLK_ENABLE();

    htim3.Instance = TIM3;
    htim3.Init.Prescaler = 84 - 1;
    htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim3.Init.Period = 100; /* 10KHz */
    htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
    ret = HAL_TIM_PWM_Init(&htim3);
    FW_ASSERT(ret != HAL_OK, TIM3_INIT_ASSERT_20, ASSERT_CRITICAL);

    sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
    sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
    ret = HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig);
    FW_ASSERT(ret != HAL_OK, TIM3_INIT_ASSERT_21, ASSERT_CRITICAL);

    sConfigOC.OCMode = TIM_OCMODE_PWM1;
    sConfigOC.Pulse = 0;
    sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
    sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
    ret = HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_2); /* Y engine LPWM */
    FW_ASSERT(ret != HAL_OK, TIM3_INIT_ASSERT_22, ASSERT_CRITICAL);

    ret = HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_3); /* Y engine RPWM */
    FW_ASSERT(ret != HAL_OK, TIM3_INIT_ASSERT_23, ASSERT_CRITICAL);

    ret = HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_4); /* X engine LPWM */
    FW_ASSERT(ret != HAL_OK, TIM3_INIT_ASSERT_24, ASSERT_CRITICAL);

    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();
    /**TIM3 GPIO Configuration
    PA7     ------> TIM3_CH2
    PB0     ------> TIM3_CH3
    PB1     ------> TIM3_CH4
    */
    GPIO_InitStruct.Pin = Y_LPWM_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Alternate = GPIO_AF2_TIM3;
    HAL_GPIO_Init(Y_LPWM_GPIO_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = Y_RPWM_Pin | X_LPWM_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Alternate = GPIO_AF2_TIM3;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    ret = HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);
    FW_ASSERT(ret != HAL_OK, ENGINES_PWM_START_ASSERT_1006, ASSERT_CRITICAL);

    ret = HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_3);
    FW_ASSERT(ret != HAL_OK, ENGINES_PWM_START_ASSERT_1007, ASSERT_CRITICAL);

    ret = HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_4);
    FW_ASSERT(ret != HAL_OK, ENGINES_PWM_START_ASSERT_1007, ASSERT_CRITICAL);
}
