#ifndef __PERIPHERAL_INIT_H
#define __PERIPHERAL_INIT_H

#ifdef __cplusplus
extern "C" {
#endif

void clock_init(void);
void gpio_init(void);
void rtc_init(void);
void spi3_init(void);
void tim2_init(void);
void tim3_init(void);
void uart1_init(void);
void tim10_counter_init(int use_irq);
void systick_disable_int(void);

#ifdef __cplusplus
}
#endif

#endif /* __PERIPHERAL_INIT_H */
