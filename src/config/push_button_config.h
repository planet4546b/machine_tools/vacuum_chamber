#ifndef __PUSH_BUTTONS_CONFIG_H
#define __PUSH_BUTTONS_CONFIG_H

#ifdef __cplusplus
extern "C" {
#endif

#include "vacuum_chamber_config.h"
#include "logger.h"

#define PUSH_BTN_CNT 2 /* It is equivalent button's IDs from 0 to (PUSH_BTN_CNT - 1) */
#define PUSH_BTN_RESET GPIO_PIN_RESET
#define PUSH_BTN_SET GPIO_PIN_SET

#define PUSH_BTN_PRESSED_GPIO_STATE PUSH_BTN_RESET
#define PUSH_BTN_RELEASED_GPIO_STATE PUSH_BTN_SET

#define PUSH_BTN_1_ID   0
#define PUSH_BTN_1_PORT (uint32_t)RED_BTN_GPIO_Port
#define PUSH_BTN_1_PIN  (uint32_t)RED_BTN_Pin
#define RED_BTN PUSH_BTN_1_ID

#define PUSH_BTN_2_ID   1
#define PUSH_BTN_2_PORT (uint32_t)ENCODER_SW_GPIO_Port
#define PUSH_BTN_2_PIN  (uint32_t)ENCODER_SW_Pin
#define ENCODER_BTN PUSH_BTN_2_ID

#if (PUSH_BUTTONS_LIB_USE_LOGGER == 1)
# define push_btn_lib_logger(...) logger_dgb_print(__VA_ARGS__)

#  if !defined(push_btn_lib_logger)
#  error "Define push_btn_lib_logger() function for buttons lib or disable PUSH_BUTTONS_LIB_USE_LOGGER!"
#  endif
#endif

/*
 * Set it depends on your loop period. For instance if we call push_buttons_proc()
 * function 1 time per 25ms, then value 2 is good. Debounce period will be equal 50ms.
 */
#define PUSH_BUTTONS_DEBOUNCE_CNT 2

#define push_btn_lib_read_pin(port, pin) HAL_GPIO_ReadPin((GPIO_TypeDef *)port, (uint16_t)pin)
#if !defined(push_btn_lib_read_pin)
# error "Define push_btn_lib_read_pin() function for buttons lib!"
#endif

#ifdef __cplusplus
}
#endif

#endif /* __PUSH_BUTTONS_CONFIG_H */
