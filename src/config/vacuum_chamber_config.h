#ifndef __VACUUM_CHAMBER_CONFIG_H
#define __VACUUM_CHAMBER_CONFIG_H

#ifdef __cplusplus
extern "C" {
#endif

#include "stm32f4xx_hal.h"
#include "FreeRTOSConfig.h"

/**********************************************************/
/*                    Device params                       */
/**********************************************************/
#define VACUUM_DEV_USE_FLASH 0

// If VACUUM_DEV_USE_FLASH==1 these params will be taken from flash memory if available
#define VACUUM_DEV_LCD_BRIGHTNESS_DEFAULT (100U)

/**********************************************************/
/*                     Threads params                     */
/**********************************************************/
#define DEBUG_TASK_PRIO (tskIDLE_PRIORITY + 1)
#define DEBUG_TASK_STACK_SIZE_WORDS (configMINIMAL_STACK_SIZE)
#define DEBUG_TASK_CYCLE_DELAY_MS (25U)
#define DEBUG_UART_SPEED 115200

#define GUI_TASK_PRIO (tskIDLE_PRIORITY + 1)
#define GUI_TASK_STACK_SIZE_WORDS (2048U)
#define GUI_TASK_CYCLE_DELAY_MS (10U)
#define GUI_TASK_HELLO_SCREEN_SHOW_TIME_MS (2000U)

/**********************************************************/
/*                   LL pin defines                       */
/**********************************************************/
#define DEBUG_LED_Pin GPIO_PIN_13
#define DEBUG_LED_GPIO_Port GPIOC
#define LCD_BRIGHT_PWM_Pin GPIO_PIN_0
#define LCD_BRIGHT_PWM_GPIO_Port GPIOA
#define LCD_DS_Pin GPIO_PIN_1
#define LCD_DS_GPIO_Port GPIOA
#define ENCODER_SW_Pin GPIO_PIN_2
#define ENCODER_SW_GPIO_Port GPIOA
#define ENCODER_A_Pin GPIO_PIN_3
#define ENCODER_A_GPIO_Port GPIOA
#define ENCODER_A_EXTI_IRQn EXTI3_IRQn
#define ENCODER_B_Pin GPIO_PIN_4
#define ENCODER_B_GPIO_Port GPIOA
#define ENCODER_B_EXTI_IRQn EXTI4_IRQn
#define RED_BTN_Pin GPIO_PIN_5
#define RED_BTN_GPIO_Port GPIOA
#define Y_LPWM_Pin GPIO_PIN_7
#define Y_LPWM_GPIO_Port GPIOA
#define Y_RPWM_Pin GPIO_PIN_0
#define Y_RPWM_GPIO_Port GPIOB
#define X_LPWM_Pin GPIO_PIN_1
#define X_LPWM_GPIO_Port GPIOB
#define X_RPWM_Pin GPIO_PIN_10
#define X_RPWM_GPIO_Port GPIOB
#define BLEED_VALVE_CLOSE_Pin GPIO_PIN_12
#define BLEED_VALVE_CLOSE_GPIO_Port GPIOB
#define BLEED_VALVE_OPEN_Pin GPIO_PIN_13
#define BLEED_VALVE_OPEN_GPIO_Port GPIOB
#define PUMP_VALVE_CLOSE_Pin GPIO_PIN_14
#define PUMP_VALVE_CLOSE_GPIO_Port GPIOB
#define PUMP_VALVE_OPEN_Pin GPIO_PIN_15
#define PUMP_VALVE_OPEN_GPIO_Port GPIOB
#define PUMP_ON_Pin GPIO_PIN_8
#define PUMP_ON_GPIO_Port GPIOA
#define BUZZER_Pin GPIO_PIN_11
#define BUZZER_GPIO_Port GPIOA
#define LCD_SPI_CS_Pin GPIO_PIN_12
#define LCD_SPI_CS_GPIO_Port GPIOA
#define LCD_RESET_Pin GPIO_PIN_15
#define LCD_RESET_GPIO_Port GPIOA
#define LCD_SPI_SCK_Pin GPIO_PIN_3
#define LCD_SPI_SCK_GPIO_Port GPIOB
#define LCD_SPI_MOSI_Pin GPIO_PIN_5
#define LCD_SPI_MOSI_GPIO_Port GPIOB

/**********************************************************/
/*                LL periph defines                       */
/**********************************************************/
#define LCD_BRIGHTNESS_TIMER TIM2

/**********************************************************/
/*                    Lib defines                         */
/**********************************************************/
#define BUZZER_USE_START_FW_BEEPING 1
#define BUZZER_START_FW_BEEP_DURATION 100
#define BUZZER_SHORT_BEEP_DURATION 80
#define BUZZER_LONG_BEEP_DURATION 200

#ifdef __cplusplus
}
#endif

#endif /* __VACUUM_CHAMBER_CONFIG_H */
