#ifndef __VACUUM_CHAMBER_GLOBAL_H
#define __VACUUM_CHAMBER_GLOBAL_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#define VC_FLAG_ON 1
#define VC_FLAG_OFF 0

typedef enum {
    PUMP_STATE = (1 << 0),
    VALVE_PUMP_STATE = (1 << 1),
    BLEED_PUMP_STATE = (1 << 2),
    DEGASSING_STATE = (1 << 3),
    BRIGHTNESS_STATE = (1 << 4),
} vc_flags_t;

typedef struct {
    uint32_t uptime;
    uint32_t flags;

    /* Degassing time params */
    uint32_t degassing_set_time;
    uint32_t degassing_cur_time;

    /* Vibrating table params */
    uint8_t vibro_x_percentage;
    uint8_t vibro_y_percentage;
} vc_dev_t;

/* Read params from flash, init global struct, prepare for degassing */
void vc_init_global_struct(void);

/* Up time API */
void vc_inc_uptime(void);
uint32_t vc_get_uptime(void);

/* Flags API */
void vc_set_flag(vc_flags_t flag, int new_state);
int vc_get_flag(vc_flags_t flag); /* Returns 0 (VC_FLAG_OFF) if flag is off */

/* Vibro API */
void vc_set_x_vibro(uint8_t percentage);
void vc_set_y_vibro(uint8_t percentage);
uint8_t vc_get_x_vibro(void);
uint8_t vc_get_y_vibro(void);

#ifdef __cplusplus
}
#endif

#endif /* __VACUUM_CHAMBER_GLOBAL_H */
