#include "vacuum_chamber_config.h"
#include "vacuum_chamber_global.h"
#include "debug_thread.h"
#include "logger.h"
#include "errors.h"

#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "semphr.h"

#include "buzzer_api.h"
#include "brightness_api.h"
#include "push_btn_api.h"
#include "push_button_config.h"
#include "pump_api.h"
#include "valves_api.h"
#include "vibration_api.h"
#include "encoder_api.h"
#include "ui.h"

#if 1
static uint8_t x_vibro = 0;

static void encoder_cw(void)
{
    x_vibro += 2;
    vc_set_x_vibro(x_vibro);
    ui_update_x_vibro();
}

static void encoder_ccw(void)
{
    x_vibro -= 2;
    vc_set_x_vibro(x_vibro);
    ui_update_x_vibro();
}

static void red_btn_click(void *args)
{
    if (vc_get_flag(PUMP_STATE)) {
        pump_off();
        vc_set_flag(PUMP_STATE, 0);
        ui_update_degassing_state("остановлена");
    } else {
        buzzer_make_ms_beep(BUZZER_SHORT_BEEP_DURATION);

        HAL_Delay(800);
        buzzer_make_ms_beep(BUZZER_LONG_BEEP_DURATION);

        HAL_Delay(800);
        buzzer_make_ms_beep(BUZZER_LONG_BEEP_DURATION);

        HAL_Delay(800);
        buzzer_make_ms_beep(BUZZER_LONG_BEEP_DURATION);

        pump_on();
        vc_set_flag(PUMP_STATE, 1);
        ui_update_degassing_state("идёт дегазация");
    }
}
static void encoder_btn_click(void *args)
{
    static int flag = 0;
    buzzer_make_ms_beep(BUZZER_SHORT_BEEP_DURATION);

    if (flag == 0) {
        flag++;
        pump_valve_close();
    } else if (flag == 1) {
        flag++;
        bleed_valve_open();
    } else if (flag == 2) {
        flag = 0;
        pump_valve_open();
        bleed_valve_close();
    }
}

#endif

static void gui_thread(void *arg)
{
    (void)arg;

    hello_screen_init();
    main_screen_init();

    hello_screen_show();
    for (int i = 0; i < GUI_TASK_HELLO_SCREEN_SHOW_TIME_MS / GUI_TASK_CYCLE_DELAY_MS; ++i) {
        lv_timer_handler();
        vTaskDelay(GUI_TASK_CYCLE_DELAY_MS / portTICK_PERIOD_MS);

        if (vc_get_flag(BRIGHTNESS_STATE) == VC_FLAG_OFF) {
#if (VACUUM_DEV_USE_FLASH == 0)
            brightness_set(VACUUM_DEV_LCD_BRIGHTNESS_DEFAULT);
#else
# error "Parameters must be read from flash!"
#endif
            vc_set_flag(BRIGHTNESS_STATE, VC_FLAG_ON);
        }
    }

    main_screen_show();
    buzzer_make_ms_beep(BUZZER_LONG_BEEP_DURATION);

#if 1
    encoder_set_cw_step_handler(encoder_cw);
    encoder_set_ccw_step_handler(encoder_ccw);

    push_btn_set_clicked_cb(RED_BTN, red_btn_click, NULL);
    push_btn_set_clicked_cb(ENCODER_BTN, encoder_btn_click, NULL);
#endif

    for (;;) {
        lv_timer_handler();

        push_btn_proc();
        encoder_proc();

        vTaskDelay(GUI_TASK_CYCLE_DELAY_MS / portTICK_PERIOD_MS);
    }
}

void gui_thread_create(void)
{
    TaskHandle_t h;

    static StaticTask_t gui_tcb;
    static StackType_t gui_stack[GUI_TASK_STACK_SIZE_WORDS];

    h = xTaskCreateStatic(gui_thread, "gui", GUI_TASK_STACK_SIZE_WORDS, NULL, GUI_TASK_PRIO, gui_stack, &gui_tcb);
    FW_ASSERT(h == NULL, ERR_THREAD_CREATION_ASSERT_501, ASSERT_CRITICAL);

    logger_dgb_print("[Thread] GUI task created successfully\r\n");
}
