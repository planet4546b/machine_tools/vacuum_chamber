#include "vacuum_chamber_config.h"
#include "vacuum_chamber_global.h"
#include "debug_thread.h"
#include "logger.h"
#include "errors.h"

#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"

#include "brightness_api.h"
#include "buzzer_api.h"
#include "push_btn_api.h"
#include "push_button_config.h"
#include "pump_api.h"
#include "valves_api.h"
#include "vibration_api.h"
#include "encoder_api.h"

#if (DEBUG_TASK_USE == 1)

# define UPTIME_LOGGING_ON 0
# define UPTIME_LOGGING_PERIOD_MS 1000

# define LCD_BRIGHTNESS_TEST_ON 0
# define LCD_BRIGHTNESS_TEST_PERIOD_MS 200

# define BUZZER_TEST_ON 0
# define BUZZER_TEST_PERIOD_MS 1000

# define PUSH_BTN_TEST_ON 0
# define PUSH_BTN_PERIOD_MS 500

# define PUMP_TEST_ON 0
# define PUMP_TEST_PERIOD_MS 500

# define VALVES_TEST_ON 0
# define VALVES_TEST_PERIOD_MS 500

# define ENGINES_TEST_ON 0
# define ENGINES_TEST_PERIOD_MS 100

# define ENCODER_TEST_ON 0
# define ENCODER_TEST_PERIOD_MS 100

# if (UPTIME_LOGGING_ON == 1)
static int uptime_logging_cycles_cnt = 0;
static void uptime_logging_cycles_inc(void) { uptime_logging_cycles_cnt++; }
static void uptime_logging_cycles_reset(void) { uptime_logging_cycles_cnt = 0; }
static int uptime_logging_cycles_get(void) { return uptime_logging_cycles_cnt; }
# endif

# if (LCD_BRIGHTNESS_TEST_ON == 1)
static int brightness_cycles_cnt = 0;
static void brightness_cycles_inc(void) { brightness_cycles_cnt++; }
static void brightness_cycles_reset(void) { brightness_cycles_cnt = 0; }
static int brightness_cycles_get(void) { return brightness_cycles_cnt; }
# endif

# if (BUZZER_TEST_ON == 1)
static int buzzer_cycles_cnt = 0;
static void buzzer_cycles_inc(void) { buzzer_cycles_cnt++; }
static void buzzer_cycles_reset(void) { buzzer_cycles_cnt = 0; }
static int buzzer_cycles_get(void) { return buzzer_cycles_cnt; }
# endif

# if (PUSH_BTN_TEST_ON == 1)
static void btn_click_cb(void *args) { buzzer_make_ms_beep(BUZZER_SHORT_BEEP_DURATION); logger_dgb_print("[dbg] I am %s\r\n", args); }
static void btn_press_cb(void *args) { (void)args; }
static void btn_release_cb(void *args) { (void)args; }
# endif

# if (PUMP_TEST_ON == 1)
static void pump_btn_press_cb(void *args) { (void)args; pump_on(); }
static void pump_btn_release_cb(void *args) { (void)args; pump_off(); }
# endif

# if (VALVES_TEST_ON == 1)
static void valve_open_btn_press_cb(void *args) { (void)args; pump_valve_open(); bleed_valve_open(); }
static void valve_close_btn_release_cb(void *args) { (void)args; pump_valve_close(); bleed_valve_close(); }
static void all_valves_stop(void *args) {(void)args; valves_turn_off_all_mosfets(); }
# endif

# if (ENGINES_TEST_ON == 1)
static uint8_t xr = 0;
static uint8_t yr = 0;

static void engine_x_btn_release_cb(void *args) { (void)args; xr = 0; motor_x_set_power(0, 0); }
static void engine_y_btn_release_cb(void *args) { (void)args; yr = 0; motor_y_set_power(0, 0); }

static void engine_x_btn_press_cb(void *args)
{
    (void)args;

    xr += 10;
    motor_x_set_power(0, xr);
}
static void engine_y_btn_press_cb(void *args)
{
    (void)args;

    yr += 10;
    motor_y_set_power(0, yr);
}
# endif

# if (ENCODER_TEST_ON == 1)
static int steps = 0;
static void encoder_cw_step(void)
{
    steps += 1;
    logger_dgb_print("[dbg][encoder] %d\r\n", steps);
}
static void encoder_ccw_step(void)
{
    steps -= 1;
    logger_dgb_print("[dbg][encoder] %d\r\n", steps);
}
# endif

static void debug_thread(void *arg)
{
    (void)arg;

# if (ENCODER_TEST_ON == 1)
    encoder_set_cw_step_handler(encoder_cw_step);
    encoder_set_ccw_step_handler(encoder_ccw_step);
# endif

# if (ENGINES_TEST_ON == 1)
    push_btn_set_pressed_cb(RED_BTN, 0, 10, engine_x_btn_press_cb, NULL);
    push_btn_set_released_cb(RED_BTN, engine_x_btn_release_cb, NULL);

    push_btn_set_pressed_cb(ENCODER_BTN, 0, 10, engine_y_btn_press_cb, NULL);
    push_btn_set_released_cb(ENCODER_BTN, engine_y_btn_release_cb, NULL);
# endif

# if (VALVES_TEST_ON == 1)
    push_btn_set_pressed_cb(RED_BTN, 0, 0, valve_open_btn_press_cb, NULL);
    push_btn_set_released_cb(RED_BTN, all_valves_stop, NULL);

    push_btn_set_pressed_cb(ENCODER_BTN, 0, 0, valve_close_btn_release_cb, NULL);
    push_btn_set_released_cb(ENCODER_BTN, all_valves_stop, NULL);
# endif

# if (PUMP_TEST_ON == 1)
    push_btn_set_pressed_cb(RED_BTN, 0, 0, pump_btn_press_cb, NULL);
    push_btn_set_released_cb(RED_BTN, pump_btn_release_cb, NULL);
# endif

# if (PUSH_BTN_TEST_ON == 1)
    push_btn_set_pressed_cb(RED_BTN, 0, 0, btn_press_cb, NULL);
    push_btn_set_pressed_cb(ENCODER_BTN, 0, 0, btn_press_cb, NULL);

    push_btn_set_released_cb(RED_BTN, btn_release_cb, NULL);
    push_btn_set_released_cb(ENCODER_BTN, btn_release_cb, NULL);

    push_btn_set_clicked_cb(RED_BTN, btn_click_cb, "RED");
    push_btn_set_clicked_cb(ENCODER_BTN, btn_click_cb, "ENCODER");
# endif

    for (;;) {
        vTaskDelay(DEBUG_TASK_CYCLE_DELAY_MS / portTICK_PERIOD_MS);

# if (ENCODER_TEST_ON == 1)
        encoder_proc();
# endif

# if (UPTIME_LOGGING_ON == 1)
        if (uptime_logging_cycles_get() >= UPTIME_LOGGING_PERIOD_MS / DEBUG_TASK_CYCLE_DELAY_MS) {
            uptime_logging_cycles_reset();
            logger_dgb_print("[dbg] uptime = %ds\r\n", vacuum_chamber_get_uptime());
        }

        uptime_logging_cycles_inc();
# endif

# if (LCD_BRIGHTNESS_TEST_ON == 1)
        static uint8_t lcd_bright = 0;
        if (brightness_cycles_get() >= LCD_BRIGHTNESS_TEST_PERIOD_MS / DEBUG_TASK_CYCLE_DELAY_MS) {
            brightness_cycles_reset();
            brightness_set(lcd_bright++);
            if (lcd_bright >= 100) {
                lcd_bright = 0;
            }
        }

        brightness_cycles_inc();
# endif

# if (BUZZER_TEST_ON == 1)
        if (buzzer_cycles_get() >= BUZZER_TEST_PERIOD_MS / DEBUG_TASK_CYCLE_DELAY_MS) {
            buzzer_make_ms_beep(BUZZER_SHORT_BEEP_DURATION);
            buzzer_cycles_reset();
        }

        buzzer_cycles_inc();
# endif
    }
}

void debug_thread_create(void)
{
    TaskHandle_t h;

    static StaticTask_t debug_tcb;
    static StackType_t debug_stack[DEBUG_TASK_STACK_SIZE_WORDS];

    h = xTaskCreateStatic(debug_thread, "dbg", DEBUG_TASK_STACK_SIZE_WORDS, NULL, DEBUG_TASK_PRIO, debug_stack, &debug_tcb);
    FW_ASSERT(h == NULL, ERR_THREAD_CREATION_ASSERT_500, ASSERT_CRITICAL);

    debug_thread_led_on();
    logger_dgb_print("[Thread] DBG task created successfully\r\n");
}

void debug_thread_led_on(void) { HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin, GPIO_PIN_RESET); }
void debug_thread_led_off(void) { HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin, GPIO_PIN_SET); }

#endif
