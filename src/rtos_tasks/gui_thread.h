#ifndef __GUI_THREAD_H
#define __GUI_THREAD_H

#ifdef __cplusplus
extern "C" {
#endif

void gui_thread_create(void);

#ifdef __cplusplus
}
#endif

#endif /* __GUI_THREAD_H */