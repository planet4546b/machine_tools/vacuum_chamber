#ifndef __DEBUG_THREAD_H
#define __DEBUG_THREAD_H

#ifdef __cplusplus
extern "C" {
#endif

void debug_thread_create(void);
void debug_thread_led_on(void);
void debug_thread_led_off(void);

#ifdef __cplusplus
}
#endif

#endif /* __DEBUG_THREAD_H */