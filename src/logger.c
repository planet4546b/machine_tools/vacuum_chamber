#include "logger.h"
#include "vacuum_chamber_config.h"

#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>

extern UART_HandleTypeDef LOGGER_UART_MODULE;

#define LOGGER_BUF_SIZE 512
static uint8_t log_buf[LOGGER_BUF_SIZE] = { 0 };

void logger_dgb_print(const char *format, ...)
{
    int32_t len = 0;

    va_list args;
    va_start(args, format);
    len = vsnprintf((char *)log_buf, sizeof(log_buf), format, args);
    va_end(args);

    HAL_UART_Transmit(&LOGGER_UART_MODULE, log_buf, len, 100);
}
