#ifndef __VALVES_API_H
#define __VALVES_API_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

void pump_valve_open(void);
void pump_valve_close(void);

void bleed_valve_open(void);
void bleed_valve_close(void);

void valves_turn_off_all_mosfets(void);

#ifdef __cplusplus
}
#endif

#endif /* __VALVES_API_H */
