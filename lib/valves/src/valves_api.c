#include "vacuum_chamber_config.h"
#include "vacuum_chamber_global.h"
#include "valves_api.h"
#include "logger.h"

void pump_valve_open(void)
{
    /* Turn off all mosfets */
    HAL_GPIO_WritePin(GPIOB, PUMP_VALVE_CLOSE_Pin | PUMP_VALVE_OPEN_Pin, GPIO_PIN_SET);

    /* Ensure triac has been closed (wait more than sin period) */
    HAL_Delay(25);

    /* OPEN valve pump == open open-valve triac */
    HAL_GPIO_WritePin(GPIOB, PUMP_VALVE_OPEN_Pin, GPIO_PIN_RESET);
}

void pump_valve_close(void)
{
    /* Turn off all mosfets */
    HAL_GPIO_WritePin(GPIOB, PUMP_VALVE_CLOSE_Pin | PUMP_VALVE_OPEN_Pin, GPIO_PIN_SET);

    /* Ensure triac has been closed (wait more than sin period) */
    HAL_Delay(25);

    /* Close valve pump == open close-valve triac */
    HAL_GPIO_WritePin(GPIOB, PUMP_VALVE_CLOSE_Pin, GPIO_PIN_RESET);
}

void bleed_valve_open(void)
{
    /* Turn off all mosfets */
    HAL_GPIO_WritePin(GPIOB, BLEED_VALVE_CLOSE_Pin | BLEED_VALVE_OPEN_Pin, GPIO_PIN_SET);

    /* Ensure triac has been closed (wait more than sin period) */
    HAL_Delay(25);

    /* OPEN valve bleed == open open-valve triac */
    HAL_GPIO_WritePin(GPIOB, BLEED_VALVE_OPEN_Pin, GPIO_PIN_RESET);
}

void bleed_valve_close(void)
{
    /* Turn off all mosfets */
    HAL_GPIO_WritePin(GPIOB, BLEED_VALVE_CLOSE_Pin | BLEED_VALVE_OPEN_Pin, GPIO_PIN_SET);

    /* Ensure triac has been closed (wait more than sin period) */
    HAL_Delay(25);

    /* Close valve bleed == open close-valve triac */
    HAL_GPIO_WritePin(GPIOB, BLEED_VALVE_CLOSE_Pin, GPIO_PIN_RESET);
}

void valves_turn_off_all_mosfets(void)
{
    HAL_GPIO_WritePin(GPIOB, BLEED_VALVE_CLOSE_Pin | BLEED_VALVE_OPEN_Pin | PUMP_VALVE_CLOSE_Pin | PUMP_VALVE_OPEN_Pin, GPIO_PIN_SET);
}
