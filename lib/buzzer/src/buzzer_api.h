#ifndef __BUZZER_H
#define __BUZZER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

void buzzer_on(void);
void buzzer_off(void);
void buzzer_make_ms_beep(uint32_t beep_ms);
void buzzer_decrement_cur_ms_beep(void);

#ifdef __cplusplus
}
#endif

#endif /* __BUZZER_H */
