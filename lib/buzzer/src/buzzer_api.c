#include "buzzer_api.h"
#include "vacuum_chamber_config.h"
#include "vacuum_chamber_global.h"

void buzzer_on(void) { BUZZER_GPIO_Port->BSRR = BUZZER_Pin; }
void buzzer_off(void) { BUZZER_GPIO_Port->BSRR = BUZZER_Pin << 16; }

static uint32_t beep_duration = 0;
void buzzer_make_ms_beep(uint32_t beep_ms)
{
    buzzer_on();
    beep_duration = beep_ms;
}

void buzzer_decrement_cur_ms_beep(void)
{
    if (beep_duration > 0) {
        --beep_duration;
    } else {
        buzzer_off();
    }
}
