#ifndef __PUSH_BTN_API_H
#define __PUSH_BTN_API_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

typedef struct {
    uint32_t id;
    uint32_t gpio_port;
    uint32_t gpio_pin;
    uint32_t pressed_time_to_cb_ms; /* 0 means the callback will be called immideatly after debounce time */
    uint32_t repeat_pressed_time_to_cb_ms; /* how ofter callback will be called if after pressed_time_to_cb_ms elapsed */

    uint32_t _is_pressed;
    uint32_t _pressed_time;
    uint32_t _repeat_pressed_time;
    uint32_t _is_simulate_click;
    uint32_t _debounce_cnt;

    void (*pressed_cb)(void *);
    void (*released_cb)(void *);
    void (*clicked_cb)(void *);

    void *pressed_data;
    void *released_data;
    void *clicked_data;
} push_btn_t;

void push_btn_proc(void);
void push_btn_set_pressed_cb(unsigned int btn_id, uint32_t pressed_time_to_cb_ms,
                             uint32_t repeat_pressed_time_to_cb_ms,
                             void (*pressed_cb)(void *),
                             void *data);
void push_btn_set_released_cb(unsigned int btn_id, void (*released_cb)(void *), void *data);
void push_btn_set_clicked_cb(unsigned int btn_id, void (*clicked_cb)(void *), void *data);
void push_btn_simulate_click(unsigned int btn_id);

#ifdef __cplusplus
} /*extern "C"*/
#endif

#endif  /* __PUSH_BTN_API_H */