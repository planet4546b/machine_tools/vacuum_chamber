#include "push_btn_api.h"
#include "push_button_config.h"

static push_btn_t btns_array[PUSH_BTN_CNT] = {
    [PUSH_BTN_1_ID] = { PUSH_BTN_1_ID, PUSH_BTN_1_PORT, PUSH_BTN_1_PIN, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL,},
    [PUSH_BTN_2_ID] = { PUSH_BTN_2_ID, PUSH_BTN_2_PORT, PUSH_BTN_2_PIN, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL,},
};

void push_btn_proc(void)
{
    for (uint32_t i = 0; i < PUSH_BTN_CNT; ++i) {
        volatile uint32_t pin_state = push_btn_lib_read_pin(btns_array[i].gpio_port, btns_array[i].gpio_pin);

        if (((pin_state == PUSH_BTN_PRESSED_GPIO_STATE) && (!btns_array[i]._is_pressed)) || \
            ((pin_state == PUSH_BTN_PRESSED_GPIO_STATE) && (btns_array[i].pressed_time_to_cb_ms != 0)) || \
            ((pin_state == PUSH_BTN_PRESSED_GPIO_STATE) && (btns_array[i].repeat_pressed_time_to_cb_ms != 0))) {

            if (!btns_array[i]._is_pressed) {
                ++btns_array[i]._debounce_cnt;
                if (btns_array[i]._debounce_cnt < PUSH_BUTTONS_DEBOUNCE_CNT) {
                    continue;
                }
            }

            btns_array[i]._is_pressed = 1;
            btns_array[i]._debounce_cnt = 0;
            ++btns_array[i]._pressed_time;

            if (btns_array[i]._pressed_time >= btns_array[i].pressed_time_to_cb_ms) {
                ++btns_array[i]._repeat_pressed_time;

                if (btns_array[i]._repeat_pressed_time >= btns_array[i].repeat_pressed_time_to_cb_ms) {
                    if (btns_array[i].pressed_cb) {
                        btns_array[i].pressed_cb(btns_array[i].pressed_data);
                        #if (PUSH_BUTTONS_LIB_USE_LOGGER == 1)
                            push_btn_lib_logger("[lib][btn][%d] pressed\n", i);
                        #endif
                    }
                    btns_array[i]._repeat_pressed_time = 0;
                }
            }
        } else if ((pin_state == PUSH_BTN_RELEASED_GPIO_STATE) && \
                   (btns_array[i]._is_pressed)) {
            btns_array[i]._is_pressed = 0;
            btns_array[i]._debounce_cnt = 0;
            btns_array[i]._pressed_time = 0;
            btns_array[i]._repeat_pressed_time = btns_array[i].repeat_pressed_time_to_cb_ms;

            if (btns_array[i].released_cb) {
                btns_array[i].released_cb(btns_array[i].released_data);
                #if (PUSH_BUTTONS_LIB_USE_LOGGER == 1)
                    push_btn_lib_logger("[lib][btn][%d] released\n", i);
                #endif
            }

            if (btns_array[i].clicked_cb) {
                btns_array[i].clicked_cb(btns_array[i].clicked_data);
                #if (PUSH_BUTTONS_LIB_USE_LOGGER == 1)
                    push_btn_lib_logger("[lib][btn][%d] clicked\n", i);
                #endif
            }
        } else {
            btns_array[i]._debounce_cnt = 0;
        }

        /* SW clicked buttons. Need for remote control or tests */
        if (btns_array[i]._is_simulate_click) {
            btns_array[i]._is_simulate_click = 0;

            if (btns_array[i].clicked_cb) {
                btns_array[i].clicked_cb(btns_array[i].clicked_data);
                #if (PUSH_BUTTONS_LIB_USE_LOGGER == 1)
                    push_btn_lib_logger("[lib][btn][%d] SW clicked\n", i);
                #endif
            }
        }
    }
}

void push_btn_set_pressed_cb(unsigned int btn_id,
                                uint32_t pressed_time_to_cb_ms,
                                uint32_t repeat_pressed_time_to_cb_ms,
                                void (*pressed_cb)(void *),
                                void *data)
{
    if ((btn_id < 0) || (btn_id >= PUSH_BTN_CNT)) {
        return;
    }

    btns_array[btn_id].pressed_time_to_cb_ms = pressed_time_to_cb_ms;
    btns_array[btn_id].repeat_pressed_time_to_cb_ms = repeat_pressed_time_to_cb_ms;
    btns_array[btn_id]._repeat_pressed_time = repeat_pressed_time_to_cb_ms;
    btns_array[btn_id].pressed_cb = pressed_cb;
    btns_array[btn_id].pressed_data = data;

    btns_array[btn_id]._is_pressed = 0;
    btns_array[btn_id]._pressed_time = 0;
    btns_array[btn_id]._is_simulate_click = 0;
    btns_array[btn_id]._debounce_cnt = 0;
}

void push_btn_set_released_cb(unsigned int btn_id, void (*released_cb)(void *), void *data)
{
    if ((btn_id < 0) || (btn_id >= PUSH_BTN_CNT)) {
        return;
    }

    btns_array[btn_id].released_cb = released_cb;
    btns_array[btn_id].released_data = data;

    btns_array[btn_id]._is_pressed = 0;
    btns_array[btn_id]._pressed_time = 0;
    btns_array[btn_id]._is_simulate_click = 0;
    btns_array[btn_id]._debounce_cnt = 0;
}

void push_btn_set_clicked_cb(unsigned int btn_id, void (*clicked_cb)(void *), void *data)
{
    if ((btn_id < 0) || (btn_id >= PUSH_BTN_CNT)) {
        return;
    }

    btns_array[btn_id].pressed_time_to_cb_ms = 0;
    btns_array[btn_id].repeat_pressed_time_to_cb_ms = 0;
    btns_array[btn_id]._repeat_pressed_time = 0;

    btns_array[btn_id].clicked_cb = clicked_cb;
    btns_array[btn_id].clicked_data = data;

    btns_array[btn_id]._is_pressed = 0;
    btns_array[btn_id]._pressed_time = 0;
    btns_array[btn_id]._is_simulate_click = 0;
    btns_array[btn_id]._debounce_cnt = 0;
}

void push_btn_simulate_click(unsigned int btn_id)
{
    btns_array[btn_id]._is_simulate_click = 1;
}