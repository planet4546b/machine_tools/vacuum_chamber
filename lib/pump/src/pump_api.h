#ifndef __PUMP_API_H
#define __PUMP_API_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

void pump_on(void);
void pump_off(void);

#ifdef __cplusplus
}
#endif

#endif /* __PUMP_API_H */
