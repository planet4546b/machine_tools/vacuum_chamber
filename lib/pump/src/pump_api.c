#include "vacuum_chamber_config.h"
#include "vacuum_chamber_global.h"
#include "pump_api.h"
#include "logger.h"

void pump_on(void)
{
    PUMP_ON_GPIO_Port->BSRR = (uint32_t)PUMP_ON_Pin << 16U;
    logger_dgb_print("[lib][pump] on\r\n");
}

void pump_off(void)
{
    PUMP_ON_GPIO_Port->BSRR = (uint32_t)PUMP_ON_Pin;
    logger_dgb_print("[lib][pump] off\r\n");
}
