#include "vacuum_chamber_config.h"
#include "vacuum_chamber_global.h"
#include "encoder_api.h"
#include "logger.h"

#include "FreeRTOS.h"
#include "task.h"

void (*cw_step)(void) = NULL;
void (*ccw_step)(void) = NULL;

enum {
    ROT_INITIAL,
    ROT_CW,
    ROT_CCW,
};

enum {
    EN_INITIAL,
    EN_WAIT_END_OF_STEP,
};

static int encoder_cur_state = EN_INITIAL;
static int encoder_cur_rot = ROT_INITIAL;
static int is_cw_step = 0;
static int is_ccw_step = 0;

void encoder_isr(void)
{
    static int a_state = 0;
    static int b_state = 0;

    a_state = (ENCODER_A_GPIO_Port->IDR & ENCODER_A_Pin) ? 1 : 0;
    b_state = (ENCODER_B_GPIO_Port->IDR & ENCODER_B_Pin) ? 1 : 0;

    if (encoder_cur_state == EN_INITIAL) { /* define rotation direction */
        if ((a_state == 1) && (b_state == 0)) {
            encoder_cur_rot = ROT_CCW;
            encoder_cur_state = EN_WAIT_END_OF_STEP;
        } else if ((b_state == 1) && (a_state == 0)) {
            encoder_cur_rot = ROT_CW;
            encoder_cur_state = EN_WAIT_END_OF_STEP;
        } else {
            encoder_cur_rot = ROT_INITIAL;
            encoder_cur_state = EN_INITIAL;
        }
    } else if (encoder_cur_state == EN_WAIT_END_OF_STEP) {
        if (encoder_cur_rot == ROT_CW) {
            uint32_t saved = taskENTER_CRITICAL_FROM_ISR();
            is_cw_step = 1;
            taskEXIT_CRITICAL_FROM_ISR(saved);
        } else if (encoder_cur_rot == ROT_CCW) {
            uint32_t saved = taskENTER_CRITICAL_FROM_ISR();
            is_ccw_step = 1;
            taskEXIT_CRITICAL_FROM_ISR(saved);
        }

        encoder_cur_state = EN_INITIAL;
        encoder_cur_rot = ROT_INITIAL;
    }
}

void encoder_set_cw_step_handler(void (*cw_handler)(void))
{
    cw_step = cw_handler;
}

void encoder_set_ccw_step_handler(void (*ccw_handler)(void))
{
    ccw_step = ccw_handler;
}

void encoder_proc(void)
{
    if (is_cw_step) {
        if (cw_step) {
            cw_step();
        }

        taskENTER_CRITICAL();
        is_cw_step = 0;
        taskEXIT_CRITICAL();
    }

    if (is_ccw_step) {
        if (ccw_step) {
            ccw_step();
        }

        taskENTER_CRITICAL();
        is_ccw_step = 0;
        taskEXIT_CRITICAL();
    }
}
