#ifndef __ENCODER_API_H
#define __ENCODER_API_H

#ifdef __cplusplus
extern "C" {
#endif

void encoder_isr(void);
void encoder_set_cw_step_handler(void (*cw_handler)(void));
void encoder_set_ccw_step_handler(void (*ccw_handler)(void));
void encoder_proc(void); /* DO NOT call from ISR */

#ifdef __cplusplus
}
#endif

#endif /* __ENCODER_API_H */
