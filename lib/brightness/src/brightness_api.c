#include "vacuum_chamber_config.h"
#include "brightness_api.h"
#include "logger.h"

void brightness_set(int percentage)
{
    if (percentage < 0) {
        percentage = 0;
    }

    if (percentage > 100) {
        percentage = 100;
    }

    LCD_BRIGHTNESS_TIMER->CCR1 = percentage;
}
