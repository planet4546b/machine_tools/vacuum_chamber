#ifndef __BRIGHTNESS_H
#define __BRIGHTNESS_H

#ifdef __cplusplus
extern "C" {
#endif

void brightness_set(int percentage); /* 0 - 100% */

#ifdef __cplusplus
}
#endif

#endif /* __BRIGHTNESS_H */
