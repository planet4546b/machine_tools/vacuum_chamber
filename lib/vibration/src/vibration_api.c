#include "vacuum_chamber_config.h"
#include "vacuum_chamber_global.h"
#include "vibration_api.h"
#include "logger.h"

// TIM2->CCR3 - X RPWM
// TIM3->CCR4 - X LPWM

// TIM3->CCR3 - Y RPWM
// TIM3->CCR2 - Y LPWM

void motor_x_set_power(uint8_t l_percentage, uint8_t r_percentage)
{
    if (l_percentage > 100) {
        l_percentage = 100;
    }

    if (r_percentage > 100) {
        r_percentage = 100;
    }

    TIM2->CCR3 = r_percentage;
    TIM3->CCR4 = l_percentage;
}

void motor_y_set_power(uint8_t l_percentage, uint8_t r_percentage)
{
    if (l_percentage > 100) {
        l_percentage = 100;
    }

    if (r_percentage > 100) {
        r_percentage = 100;
    }

    TIM3->CCR3 = r_percentage;
    TIM3->CCR2 = l_percentage;
}

void motors_off(void)
{
    TIM2->CCR3 = 0;
    TIM3->CCR4 = 0;
    TIM3->CCR3 = 0;
    TIM3->CCR2 = 0;
}
