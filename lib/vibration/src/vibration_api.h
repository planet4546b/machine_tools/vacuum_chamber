#ifndef __VIBRATION_API_H
#define __VIBRATION_API_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

void motor_x_set_power(uint8_t l_percentage, uint8_t r_percentage);
void motor_y_set_power(uint8_t l_percentage, uint8_t r_percentage);
void motors_off(void);

#ifdef __cplusplus
}
#endif

#endif /* __VIBRATION_API_H */
